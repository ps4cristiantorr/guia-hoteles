 
  $(function () {
        $('#modalContact').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget);
          var email = button.data('email') 
          var titleHotel = button.data('hotel') 
          var modal = $(this)
          modal.find('.modal-title').text('Te enviaremos más información sobre ' + titleHotel + '!!')
          modal.find('.modal-body #email').val(email);
          modal.find('.modal-body #hotel').val(titleHotel);
          $('#btnModalContact').removeClass('btn-modal-contact ');
          $('#btnModalContact').addClass('btn-danger');
          $('#btnModalContact').prop('disabled', true );
      });
     
      $('#modalContact').on('shown.bs.modal', function (event) {
        console.log('Se término de mostrar');
      });

      $('#modalContact').on('hidden.bs.modal', function (e) {
        console.log('Se oculto');
      })
      $('#modalContact').on('hidden.bs.modal', function (e) {
        console.log('Se término de ocultar');
        $('#btnModalContact').addClass('btn-modal-contact ');
         $('#btnModalContact').removeClass('btn-danger');
         $('#btnModalContact').prop('disabled', false );
      })
   });
   
    $(function () {
      $('[data-toggle="tooltip"]').tooltip();
      $('[data-toggle="popover"]').popover()
    });

  $(function () {
      $('.carousel').carousel({
        interval: 4000
      })
  });